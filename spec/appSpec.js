"use strict";
//https://github.com/dwyl/learn-nightwatch
let tester = function (browser, time, expectedTheme) {
    browser
        .url(`http://localhost:3000?hour=${time}`)
        .waitForElementVisible('body', 1000)
        .getTitle(function (title) {
            console.log(`The title is ${title.toLowerCase()}`);
            this.verify.equal(title.toLowerCase().indexOf(expectedTheme) !== -1,
                true,
                `Title should contain ${expectedTheme} during ${expectedTheme}time hours, including ${time}`);
        });
};

const child_process = require('child_process');
let runningApp;
module.exports = {
    before: function (browser, done) {
        console.log("Starting application locally");
        runningApp = child_process.spawn('node', ['app.js']);
        runningApp.stdout.on('data', (data) => {
            console.log(`stdout: ${data}`);
        });
        runningApp.stderr.on('data', (data) => {
            console.log(`stderr: ${data}`);
        });
        console.log(runningApp.pid);
        done();
    },
    after: function (browser, done) {
        runningApp.kill('SIGINT');
        done();
    },
    'test daytime theme': function (browser) {
        tester(browser, 7, "day");
        tester(browser, 10, "day");
        tester(browser, 14, "day");
        tester(browser, 16, "day");
    },
    'test night theme': function (browser) {
        tester(browser, 0, "night");
        tester(browser, 1, "night");
        tester(browser, 6, "night");
        tester(browser, 19, "night");
        tester(browser, 23, "night");
    },
    'test dynamic theme': function (browser) {
        let currentHour = (new Date()).getHours();
        let dayOrNight = (currentHour >= 7 && currentHour < 19) ? "day" : "night";
        console.log(`Current time is ${currentHour}, which makes it ${dayOrNight}time.`);
        tester(browser, currentHour, dayOrNight);
    },
    'test that css theme changes': function (browser) {
        let storedBackground = '';
        console.log("Testing that CSS background of body is different in day and night");
        browser
            .url(`http://localhost:3000?hour=8`)
            .waitForElementVisible('body', 1000)
            .getCssProperty('body', "background", function (result) {
                console.log(result);
                storedBackground = result.value;
                console.log(`Background during day: ${storedBackground}`);
            })
            .url(`http://localhost:3000?hour=21`)
            .waitForElementVisible('body', 1000)
            .getCssProperty('body', "background", function (result) {
                let nightBg = result.value;
                console.log(`Background during night: ${nightBg}`);
                this.verify.equal(storedBackground !== nightBg, true);
            });
    },
    'quit': function (browser) {
        browser.end();
    }
};
