"use strict";

const express = require('express');
const hoffman = require('hoffman');
const path = require('path');

const app = express(); //express module returns a function, not an object!
app.set('views', path.join(__dirname, 'views')); // path to your templates
app.set('view engine', 'dust');
app.engine('dust', hoffman.__express());

app.get("/", function (request, response) {
    let query = request.query;
    console.log(`This was the query:`);
    console.log(query);
    console.log(`This was the request URL: ${request.url}`);
    let hour = query.hour; //query["hour"];
    let minute = query.minute; //query["minute"];
    console.log(`The hour is ${hour} and the minute is ${minute}`);
    let currentDateTime = new Date();
    if (currentDateTime.getHours() > 7) {
        response.render('index', {
            greeting: `Hello, world! It is now ${currentDateTime.getHours()}`,
            style: 'styles'
        });
    } else {
        response.render('index', {
            greeting: "Go away, I'm sleeping!",
            style: 'altStyle'
        });
    }
});

app.use('/assets', express.static('assets'));

app.listen(3000, function () {
    console.log("My app is listening on port 3000!");
});
