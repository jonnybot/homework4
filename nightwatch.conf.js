"use strict";
module.exports = (function (settings) {
    let selenium = require('selenium-server-standalone-jar');
    settings.selenium.server_path = selenium.path;
    settings.selenium["cli_args"] = {
        "webdriver.chrome.driver": "",
        "webdriver.gecko.driver": "",
        "webdriver.edge.driver": ""
    };
    return settings;
})(require('./nightwatch.json'));